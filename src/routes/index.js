const express = require('express')
const router = express.Router()
const config = require('../config/config')
const getUrlPrefix = config.app.prefix

const CreateUserController = require('../controller/CreateUser');

/* GET home page. */
router.get(getUrlPrefix + '/ping', (req, res) => {
  res.status(200).send('pong')
})


router.post(getUrlPrefix+'/createuser', (req, res)=> {
  CreateUserController.CreateUser(req,res)
})


router.get("/keepalive", (req, res, next) => {
  try {
    responsedata = "OK";
    res.status(200).json(responsedata);
  } catch (error) {
    res.send(error);
  }
});

module.exports = router
