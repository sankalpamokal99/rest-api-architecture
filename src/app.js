const express = require('express')
const path = require('path')
const indexRouter = require('./routes/index')
const config = require('./config/config')
const getUrlPrefix = config.app.prefix
const cors = require('cors')
const app = express()
var bodyParser = require('body-parser')
const fileUpload = require('express-fileupload');
const session = require('express-session')

/* GLOBAL VARIABLES */

global.connectDatabase = require('./utils/MySQL/dbConn').connectDatabase
global.dbQuery = require('./model/MySQLQuery/mySqlQuery')
global.msgCodeJson = require('./utils/MsgCode/msgCode')
global.httpResponseHandlerError = require('./services/httpResponseHandler').httpResponseHandlerError
global.httpResponseSuccessHandler = require('./services/httpResponseHandler').httpResponseSuccessHandler
global.q = require('q')


// const sessionOptions = {
//   secret: config.app.secret,
//   resave: false,
//   saveUninitialized: false,
//   cookie: { maxAge: 600000 ,secure: false,httpOnly: true }
// }

// app.use(session(sessionOptions))

app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(fileUpload({ limits: { fileSize: 50 * 1024 * 1024 } }))

// global.uploadFile=require('./controller/uploadFile').uploadFile;

const whitelist = config.whiteListUrl
const NODE_ENV = process.env.NODE_ENV
if (!(NODE_ENV == 'production' || NODE_ENV == 'prod')) {
  var corsOptions = {
    origin: function (origin, callback) {
      //  if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
      //  } else {
      //    callback(new Error('Not allowed by CORS'))
      //  }
    },
    credentials: true, //Credentials are cookies, authorization headers or TLS client certificates.
  };
  app.use(cors(corsOptions))
}

app.use('/', indexRouter)

/* SERVER START */
const port = process.env.PORT || config.server.port
const server = app.listen(port, () => {
  console.log("Listening for requests on port", port);
});

module.exports = app



